package com.alibaba.maven.plugin.docbook;

/**
 * Mojo does nothing to make eclipse happy.
 * 
 * @author Michael Zhou
 * @goal noop
 */
public class NoopMojo {
}
